import article from "./appStateChangeReducer/article";
import articleList from "./appStateChangeReducer/articleList";
import auth from "./appStateChangeReducer/auth";
import { combineReducers } from "redux";
import common from "./appStateChangeReducer/common";
import editor from "./appStateChangeReducer/editor";
import home from "./appStateChangeReducer/home";
import profile from "./appStateChangeReducer/profile";
import { routerReducer } from "react-router-redux";

export default combineReducers({
  article,
  articleList,
  auth,
  common,
  editor,
  home,
  profile,
  router: routerReducer,
});
