import { Profile, bindStateToProps } from "./Profile";
import React from "react";
import { Link } from "react-router-dom";
import httpInterceptor from "../httpInterceptor";
import { connect } from "react-redux";
import { PROFILE_LOADED, PROFILE_UNLOADED } from "./Constants";

const stateDispatchToProps = (dispatch) => ({
  onLoad: (pager, payload) =>
    dispatch({ type: PROFILE_LOADED, pager, payload }),
  onUnload: () => dispatch({ type: PROFILE_UNLOADED }),
});

class ProfileFavorites extends Profile {
  componentWillMount() {
    this.props.onLoad(
      (page) =>
        httpInterceptor.Articles.favoritedBy(
          this.props.match.params.username,
          page
        ),
      Promise.all([
        httpInterceptor.Profile.get(this.props.match.params.username),
        httpInterceptor.Articles.favoritedBy(this.props.match.params.username),
      ])
    );
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  renderTabs() {
    return (
      <ul className="nav nav-pills outline-active">
        <li className="nav-item">
          <Link className="nav-link" to={`/@${this.props.profile.username}`}>
            My Articles
          </Link>
        </li>

        <li className="nav-item">
          <Link
            className="nav-link active"
            to={`/@${this.props.profile.username}/favorites`}
          >
            Favorited Articles
          </Link>
        </li>
      </ul>
    );
  }
}

export default connect(
  bindStateToProps,
  stateDispatchToProps
)(ProfileFavorites);
