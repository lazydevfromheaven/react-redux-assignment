import {
  ARTICLES_LOADED,
  ARTICLES_UNLOADED,
  ADD_COMMENT,
  DELETE_COMMENT,
} from "../components/Constants";

export default (state = {}, action) => {
  switch (action.type) {
    case ARTICLES_LOADED:
      return {
        ...state,
        article: action.payload[0].article,
        comments: action.payload[1].comments,
      };
    case ARTICLES_UNLOADED:
      return {};
    case ADD_COMMENT:
      return {
        ...state,
        commentErrors: action.error ? action.payload.errors : null,
        comments: action.error
          ? null
          : (state.comments || []).concat([action.payload.comment]),
      };
    case DELETE_COMMENT:
      const commentId = action.commentId;
      return {
        ...state,
        comments: state.comments.filter((comment) => comment.id !== commentId),
      };
    default:
      return state;
  }
};
