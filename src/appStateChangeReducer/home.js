import { HOME_PAGE_INIT, HOME_PAGE_UNLOADED } from "../components/Constants";

export default (state = {}, action) => {
  switch (action.type) {
    case HOME_PAGE_INIT:
      return {
        ...state,
        tags: action.payload[0].tags,
      };
    case HOME_PAGE_UNLOADED:
      return {};
    default:
      return state;
  }
};
