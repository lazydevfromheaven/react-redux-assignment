import SiteTopNav from "./SiteTopNav";
import React from "react";
import { connect } from "react-redux";
import { push } from "react-router-redux";
import httpInterceptor from "../httpInterceptor";
import { APP_INIT, REDIRECT } from "./Constants";
import { Route, Switch } from "react-router-dom";
import Article from "../components/Article";
import Editor from "./PublisherView";
import Home from "../components/Home";
import UserLogin from "./UserLogin";
import Profile from "../components/Profile";
import ProfileFavorites from "../components/ProfileFavorites";
import Register from "./SignInView";
import { store } from "../store";

const bindStateToProps = (state) => {
  return {
    appLoaded: state.common.appLoaded,
    appName: state.common.appName,
    currentUser: state.common.currentUser,
    redirectTo: state.common.redirectTo,
  };
};

const stateDispatchToProps = (dispatch) => ({
  onLoad: (payload, token) =>
    dispatch({ type: APP_INIT, payload, token, skipTracking: true }),
  onRedirect: () => dispatch({ type: REDIRECT }),
});

class App extends React.Component {
  componentWillReceiveProps(componentProps) {
    if (componentProps.redirectTo) {
      store.dispatch(push(componentProps.redirectTo));
      this.props.onRedirect();
    }
  }

  componentWillMount() {
    const accessToken = window.localStorage.getItem("accessToken");
    if (accessToken) {
      httpInterceptor.setToken(accessToken);
    }

    this.props.onLoad(
      accessToken ? httpInterceptor.Auth.current() : null,
      accessToken
    );
  }

  render() {
    if (this.props.appLoaded) {
      return (
        <div>
          <SiteTopNav
            appName={this.props.appName}
            currentUser={this.props.currentUser}
          />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/userlogin" component={UserLogin} />
            <Route path="/register" component={Register} />
            <Route path="/editor/:slug" component={Editor} />
            <Route path="/editor" component={Editor} />
            <Route path="/article/:id" component={Article} />
            <Route path="/@:username/favorites" component={ProfileFavorites} />
            <Route path="/@:username" component={Profile} />
          </Switch>
        </div>
      );
    }
    return (
      <div>
        <SiteTopNav
          appName={this.props.appName}
          currentUser={this.props.currentUser}
        />
      </div>
    );
  }
}

export default connect(bindStateToProps, stateDispatchToProps)(App);
