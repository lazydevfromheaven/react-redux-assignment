import {
  APP_INIT,
  REDIRECT,
  LOGOUT,
  ARTICLE_PUBLISHED,
  USER_SETTINGS,
  LOGIN,
  REGISTER,
  DELETE_ARTICLE,
  ARTICLES_UNLOADED,
  EDITOR_PAGE_UNLOADED,
  HOME_PAGE_UNLOADED,
  PROFILE_UNLOADED,
  PROFILE_FAV_UNLOAD,
  SETTINGS_UNLOADED,
  LOGIN_UNLOADED,
  REGISTER_UNLOADED,
} from "../components/Constants";

const defaultState = {
  appName: "Nagarro Assignment",
  token: null,
  viewChangeCounter: 0,
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case APP_INIT:
      return {
        ...state,
        token: action.token || null,
        appLoaded: true,
        currentUser: action.payload ? action.payload.user : null,
      };
    case REDIRECT:
      return { ...state, redirectTo: null };
    case LOGOUT:
      return { ...state, redirectTo: "/", token: null, currentUser: null };
    case ARTICLE_PUBLISHED:
      const redirectUrl = `/article/${action.payload.article.slug}`;
      return { ...state, redirectTo: redirectUrl };
    case USER_SETTINGS:
      return {
        ...state,
        redirectTo: action.error ? null : "/",
        currentUser: action.error ? null : action.payload.user,
      };
    case LOGIN:
    case REGISTER:
      return {
        ...state,
        redirectTo: action.error ? null : "/",
        token: action.error ? null : action.payload.user.token,
        currentUser: action.error ? null : action.payload.user,
      };
    case DELETE_ARTICLE:
      return { ...state, redirectTo: "/" };
    case ARTICLES_UNLOADED:
    case EDITOR_PAGE_UNLOADED:
    case HOME_PAGE_UNLOADED:
    case PROFILE_UNLOADED:
    case PROFILE_FAV_UNLOAD:
    case SETTINGS_UNLOADED:
    case LOGIN_UNLOADED:
    case REGISTER_UNLOADED:
      return { ...state, viewChangeCounter: state.viewChangeCounter + 1 };
    default:
      return state;
  }
};
