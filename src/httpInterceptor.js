import superagentPromise from "superagent-promise";
import _superagent from "superagent";

const superagent = superagentPromise(_superagent, global.Promise);
let token = null;
const tokenObject = (req) => {
  if (token) {
    req.set("authorization", `Token ${token}`);
  }
};
//const PROD_API = "localhost:8080/api/";
const PROD_API = "https://conduit.productionready.io/api";
const apiRequests = {
  del: (url) =>
    superagent.del(`${PROD_API}${url}`).use(tokenObject).then(responseBody),
  get: (url) =>
    superagent.get(`${PROD_API}${url}`).use(tokenObject).then(responseBody),
  put: (url, body) =>
    superagent
      .put(`${PROD_API}${url}`, body)
      .use(tokenObject)
      .then(responseBody),
  post: (url, body) =>
    superagent
      .post(`${PROD_API}${url}`, body)
      .use(tokenObject)
      .then(responseBody),
};

const encode = encodeURIComponent;
const responseBody = (res) => res.body;

const Auth = {
  current: () => apiRequests.get("/user"),
  login: (email, password) =>
    apiRequests.post("/users/login", { user: { email, password } }),
  register: (username, email, password) =>
    apiRequests.post("/users", { user: { username, email, password } }),
  save: (user) => apiRequests.put("/user", { user }),
};

const Tags = {
  getAll: () => apiRequests.get("/tags"),
};

const limit = (count, p) => `limit=${count}&offset=${p ? p * count : 0}`;
const omitSlug = (article) => Object.assign({}, article, { slug: undefined });
const Articles = {
  all: (page) => apiRequests.get(`/articles?${limit(10, page)}`),
  feed: () => apiRequests.get("/articles/feed?limit=10&offset=0"),
  get: (slug) => apiRequests.get(`/articles/${slug}`),
  create: (article) => apiRequests.post("/articles", { article }),
  favorite: (slug) => apiRequests.post(`/articles/${slug}/favorite`),
  unfavorite: (slug) => apiRequests.del(`/articles/${slug}/favorite`),
  byAuthor: (author, page) =>
    apiRequests.get(`/articles?author=${encode(author)}&${limit(5, page)}`),
  byTag: (tag, page) =>
    apiRequests.get(`/articles?tag=${encode(tag)}&${limit(10, page)}`),
  del: (slug) => apiRequests.del(`/articles/${slug}`),
  favoritedBy: (author, page) =>
    apiRequests.get(`/articles?favorited=${encode(author)}&${limit(5, page)}`),

  update: (article) =>
    apiRequests.put(`/articles/${article.slug}`, {
      article: omitSlug(article),
    }),
};
const Profile = {
  follow: (username) => apiRequests.post(`/profiles/${username}/follow`),
  get: (username) => apiRequests.get(`/profiles/${username}`),
  unfollow: (username) => apiRequests.del(`/profiles/${username}/follow`),
};
const Comments = {
  create: (slug, comment) =>
    apiRequests.post(`/articles/${slug}/comments`, { comment }),
  delete: (slug, commentId) =>
    apiRequests.del(`/articles/${slug}/comments/${commentId}`),
  forArticle: (slug) => apiRequests.get(`/articles/${slug}/comments`),
};

export default {
  Articles,
  Auth,
  Comments,
  Profile,
  Tags,
  setToken: (_token) => {
    token = _token;
  },
};
