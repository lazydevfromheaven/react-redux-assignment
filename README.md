# react-redux-assignment

This assignment is part of training for React and Redux

Assumptions:
1. I have referred the HTML/CSS design from the website 
    https://demo.productionready.io
2. Have Used localstorage for Authentication/Authorization with each API request
3. I've added support for **tags and marking an article as favourite and unfavourite**
4. I've referred to the react-redux official documentation on how to authorize all the API calls using middleware, store and reducers.
