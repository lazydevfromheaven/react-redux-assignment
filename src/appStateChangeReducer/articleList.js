import {
  ARTICLE_FAV,
  ARTICLE_UNFAV,
  SET_PAGE,
  APPLY_TAG_FILTER,
  HOME_PAGE_INIT,
  HOME_PAGE_UNLOADED,
  CHANGE_TAB,
  PROFILE_LOADED,
  PROFILE_UNLOADED,
  PROFILE_FAV_LOAD,
  PROFILE_FAV_UNLOAD,
} from "../components/Constants";

export default (state = {}, action) => {
  switch (action.type) {
    case ARTICLE_FAV:
    case ARTICLE_UNFAV:
      return {
        ...state,
        articles: state.articles.map((article) => {
          if (article.slug === action.payload.article.slug) {
            return {
              ...article,
              favorited: action.payload.article.favorited,
              favoritesCount: action.payload.article.favoritesCount,
            };
          }
          return article;
        }),
      };
    case SET_PAGE:
      return {
        ...state,
        articles: action.payload.articles,
        articlesCount: action.payload.articlesCount,
        currentPage: action.page,
      };
    case APPLY_TAG_FILTER:
      return {
        ...state,
        pager: action.pager,
        articles: action.payload.articles,
        articlesCount: action.payload.articlesCount,
        tab: null,
        tag: action.tag,
        currentPage: 0,
      };
    case HOME_PAGE_INIT:
      return {
        ...state,
        pager: action.pager,
        tags: action.payload[0].tags,
        articles: action.payload[1].articles,
        articlesCount: action.payload[1].articlesCount,
        currentPage: 0,
        tab: action.tab,
      };
    case HOME_PAGE_UNLOADED:
      return {};
    case CHANGE_TAB:
      return {
        ...state,
        pager: action.pager,
        articles: action.payload.articles,
        articlesCount: action.payload.articlesCount,
        tab: action.tab,
        currentPage: 0,
        tag: null,
      };
    case PROFILE_LOADED:
    case PROFILE_FAV_LOAD:
      return {
        ...state,
        pager: action.pager,
        articles: action.payload[1].articles,
        articlesCount: action.payload[1].articlesCount,
        currentPage: 0,
      };
    case PROFILE_UNLOADED:
    case PROFILE_FAV_UNLOAD:
      return {};
    default:
      return state;
  }
};
