import { Link } from "react-router-dom";
import React from "react";
import httpInterceptor from "../../httpInterceptor";
import { connect } from "react-redux";
import { DELETE_ARTICLE } from "../Constants";

const stateDispatchToProps = (dispatch) => ({
  onClickDelete: (payload) => dispatch({ type: DELETE_ARTICLE, payload }),
});

const ModifyArticle = (props) => {
  const article = props.article;
  const del = () => {
    props.onClickDelete(httpInterceptor.Articles.del(article.slug));
  };
  if (props.canModify) {
    return (
      <span>
        <Link
          to={`/editor/${article.slug}`}
          className="btn btn-sm btn-outline-secondary"
        >
          <i className="ion-edit"></i> Edit Article
        </Link>

        <button className="btn btn-sm btn-outline-danger" onClick={del}>
          <i className="ion-trash-a"></i> Delete Article
        </button>
      </span>
    );
  }

  return <span></span>;
};

export default connect(() => ({}), stateDispatchToProps)(ModifyArticle);
