import MainView from "./MainView";
import React from "react";
import Tags from "./Tags";
import httpInterceptor from "../../httpInterceptor";
import { connect } from "react-redux";
import {
  HOME_PAGE_INIT,
  HOME_PAGE_UNLOADED,
  APPLY_TAG_FILTER,
} from "../Constants";

const Promise = global.Promise;

const bindStateToProps = (state) => ({
  ...state.home,
  appName: state.common.appName,
  token: state.common.token,
});

const stateDispatchToProps = (dispatch) => ({
  onClickTag: (tag, pager, payload) =>
    dispatch({ type: APPLY_TAG_FILTER, tag, pager, payload }),
  onLoad: (tab, pager, payload) =>
    dispatch({ type: HOME_PAGE_INIT, tab, pager, payload }),
  onUnload: () => dispatch({ type: HOME_PAGE_UNLOADED }),
});

class Home extends React.Component {
  componentWillMount() {
    const tab = this.props.token ? "feed" : "all";
    const articlesPromise = this.props.token
      ? httpInterceptor.Articles.feed
      : httpInterceptor.Articles.all;

    this.props.onLoad(
      tab,
      articlesPromise,
      Promise.all([httpInterceptor.Tags.getAll(), articlesPromise()])
    );
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <div className="home-page">
        <div className="banner">
          <div className="container">
            <h1 className="logo-font">React Assignment</h1>
            <p>Assignment submitted by 3147090.</p>
          </div>
        </div>

        <div className="container page">
          <div className="row">
            <MainView />

            <div className="col-md-3">
              <div className="sidebar">
                <p>Popular Tags</p>

                <Tags
                  tags={this.props.tags}
                  onClickTag={this.props.onClickTag}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(bindStateToProps, stateDispatchToProps)(Home);
