import { Link } from "react-router-dom";
import React from "react";
import httpInterceptor from "../httpInterceptor";
import { connect } from "react-redux";
import { UPDATE_AUTH, LOGIN, LOGIN_UNLOADED } from "./Constants";

const bindStateToProps = (state) => ({ ...state.auth });

const stateDispatchToProps = (dispatch) => ({
  onChangeEmail: (value) =>
    dispatch({ type: UPDATE_AUTH, key: "email", value }),
  onChangePassword: (value) =>
    dispatch({ type: UPDATE_AUTH, key: "password", value }),
  onSubmit: (email, password) =>
    dispatch({
      type: LOGIN,
      payload: httpInterceptor.Auth.login(email, password),
    }),
  onUnload: () => dispatch({ type: LOGIN_UNLOADED }),
});

class UserLogin extends React.Component {
  constructor() {
    super();
    this.changeEmail = (ev) => this.props.onChangeEmail(ev.target.value);
    this.changePassword = (ev) => this.props.onChangePassword(ev.target.value);
    this.submitForm = (email, password) => (ev) => {
      ev.preventDefault();
      this.props.onSubmit(email, password);
    };
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const email = this.props.email;
    const password = this.props.password;
    return (
      <div className="auth-page">
        <div className="container page">
          <div className="row">
            <div className="col-md-6 offset-md-3 col-xs-12">
              <h1 className="text-xs-center">Sign In</h1>
              <p className="text-xs-center">
                <Link to="/register">Need an account?</Link>
              </p>
              <form onSubmit={this.submitForm(email, password)}>
                <fieldset>
                  <fieldset className="form-group">
                    <input
                      className="form-control form-control-lg"
                      type="email"
                      placeholder="Email"
                      value={email}
                      onChange={this.changeEmail}
                    />
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control form-control-lg"
                      type="password"
                      placeholder="Password"
                      value={password}
                      onChange={this.changePassword}
                    />
                  </fieldset>

                  <button
                    className="btn btn-lg btn-primary pull-xs-right"
                    type="submit"
                    disabled={this.props.inProgress}
                  >
                    Sign in
                  </button>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(bindStateToProps, stateDispatchToProps)(UserLogin);
