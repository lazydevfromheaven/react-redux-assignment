import React from "react";
import httpInterceptor from "../httpInterceptor";
import { connect } from "react-redux";
import { SET_PAGE } from "./Constants";

const stateDispatchToProps = (dispatch) => ({
  onSetPage: (page, payload) => dispatch({ type: SET_PAGE, page, payload }),
});

const ArticleNavigation = (props) => {
  if (props.articlesCount <= 10) {
    return null;
  }

  const totalArticles = [];
  for (let i = 0; i < Math.ceil(props.articlesCount / 10); ++i) {
    totalArticles.push(i);
  }

  const setPage = (page) => {
    if (props.pager) {
      props.onSetPage(page, props.pager(page));
    } else {
      props.onSetPage(page, httpInterceptor.Articles.all(page));
    }
  };

  return (
    <nav>
      <ul className="pagination">
        {totalArticles.map((v) => {
          const isCurrent = v === props.currentPage;
          const onClick = (ev) => {
            ev.preventDefault();
            setPage(v);
          };
          return (
            <li
              className={isCurrent ? "page-item active" : "page-item"}
              onClick={onClick}
              key={v.toString()}
            >
              <a className="page-link" href="">
                {v + 1}
              </a>
            </li>
          );
        })}
      </ul>
    </nav>
  );
};

export default connect(() => ({}), stateDispatchToProps)(ArticleNavigation);
