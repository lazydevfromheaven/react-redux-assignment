import ModifyArticle from "./ModifyArticle";
import { Link } from "react-router-dom";
import React from "react";

const ArticleView = (props) => {
  const article = props.article;
  return (
    <div className="article-meta">
      <Link to={`/@${article.author.username}`}>
        <img src={article.author.image} alt={article.author.username} />
      </Link>

      <div className="info">
        <Link to={`/@${article.author.username}`} className="author">
          {article.author.username}
        </Link>
        <span className="date">
          {new Date(article.createdAt).toDateString()}
        </span>
      </div>

      <ModifyArticle canModify={props.canModify} article={article} />
    </div>
  );
};

export default ArticleView;
