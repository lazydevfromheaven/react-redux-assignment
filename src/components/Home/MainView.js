import ArticleList from "../ArticleList";
import React from "react";
import httpInterceptor from "../../httpInterceptor";
import { connect } from "react-redux";
import { CHANGE_TAB } from "../Constants";

const MyOwnFeed = (props) => {
  if (props.token) {
    const clickHandler = (ev) => {
      ev.preventDefault();
      props.onTabClick(
        "feed",
        httpInterceptor.Articles.feed,
        httpInterceptor.Articles.feed()
      );
    };

    return (
      <li className="nav-item">
        <a
          href=""
          className={props.tab === "feed" ? "nav-link active" : "nav-link"}
          onClick={clickHandler}
        >
          My Feed
        </a>
      </li>
    );
  }
  return null;
};

const AllFeed = (props) => {
  const clickHandler = (ev) => {
    ev.preventDefault();
    props.onTabClick(
      "all",
      httpInterceptor.Articles.all,
      httpInterceptor.Articles.all()
    );
  };
  return (
    <li className="nav-item">
      <a
        href=""
        className={props.tab === "all" ? "nav-link active" : "nav-link"}
        onClick={clickHandler}
      >
        Global Feed
      </a>
    </li>
  );
};

const TagFilterTab = (props) => {
  if (!props.tag) {
    return null;
  }

  return (
    <li className="nav-item">
      <a href="" className="nav-link active">
        <i className="ion-pound"></i> {props.tag}
      </a>
    </li>
  );
};

const bindStateToProps = (state) => ({
  ...state.articleList,
  tags: state.home.tags,
  token: state.common.token,
});

const stateDispatchToProps = (dispatch) => ({
  onTabClick: (tab, pager, payload) =>
    dispatch({ type: CHANGE_TAB, tab, pager, payload }),
});

const MainView = (props) => {
  return (
    <div className="col-md-9">
      <div className="feed-toggle">
        <ul className="nav nav-pills outline-active">
          <AllFeed tab={props.tab} onTabClick={props.onTabClick} />
          <MyOwnFeed
            token={props.token}
            tab={props.tab}
            onTabClick={props.onTabClick}
          />
          <TagFilterTab tag={props.tag} />
        </ul>
      </div>

      <ArticleList
        pager={props.pager}
        articles={props.articles}
        loading={props.loading}
        articlesCount={props.articlesCount}
        currentPage={props.currentPage}
      />
    </div>
  );
};

export default connect(bindStateToProps, stateDispatchToProps)(MainView);
