import React from "react";
import httpInterceptor from "../../httpInterceptor";
import { connect } from "react-redux";
import { DELETE_COMMENT } from "../Constants";

const stateDispatchToProps = (dispatch) => ({
  onClick: (payload, commentId) =>
    dispatch({ type: DELETE_COMMENT, payload, commentId }),
});

const ModifyComment = (props) => {
  const del = () => {
    const payload = httpInterceptor.Comments.delete(
      props.slug,
      props.commentId
    );
    props.onClick(payload, props.commentId);
  };

  if (props.show) {
    return (
      <span className="mod-options">
        <i className="ion-trash-a" onClick={del}></i>
      </span>
    );
  }
  return null;
};

export default connect(() => ({}), stateDispatchToProps)(ModifyComment);
